﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Peti__sesti_i_sedmi_zadatak7
{
    class Book: IItem
    {
        public string Title { get; private set; }
        public double Price { get; private set; }
        public Book(string title, double price)
        {
            this.Title = title;
            this.Price = price;
        }
        public override string ToString()
        {
            return "Book: " + this.Title + Environment.NewLine + " -> Price:" + this.Price;
        }
        public double Accept(IVisitor Visitor)
        {
            return Visitor.Visit(this);

        }
    }
}
