﻿using System;

namespace Peti__sesti_i_sedmi_zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            BuyVisitor buyer = new BuyVisitor();
            Book firstBook = new Book("Mali princ", 50);
            DVD firstDVD = new DVD("Office", DVDType.SOFTWARE, 20);

            Console.WriteLine(firstBook.Accept(buyer));
            Cart cart = new Cart();
            cart.SetVisitor(buyer);
            cart.AddItem(firstBook);
            cart.AddItem(firstDVD);

            Console.WriteLine(cart.Accept());
        }
    }
}
