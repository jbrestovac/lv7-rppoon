﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Peti__sesti_i_sedmi_zadatak7
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
