﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Peti__sesti_i_sedmi_zadatak7
{
    interface IVisitor
    {
        double Visit(DVD DVDItem);
        double Visit(VHS VHSItem);
        double Visit(Book BookItem);
    }
}
