﻿using System;

namespace Prvi_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ////1. zadatak
            double[] array = new double[] { 9887, 1, 52213, -157.1, 2, 59, -789, 2110, 99, 457, 11 };
            NumberSequence sequence = new NumberSequence(array);
            //BubbleSort bubble = new BubbleSort();
            //SequentialSort sequential = new SequentialSort();
            //CombSort combsort = new CombSort();

            //sequence.SetSortStrategy(bubble);
            //Console.WriteLine("Before:\n" + sequence.ToString());

            //sequence.Sort();
            //Console.WriteLine("After bubblesort:\n" + sequence.ToString());


            //sequence.SetSortStrategy(sequential);
            //sequence.Sort();
            //Console.WriteLine("After sequentialsort:\n" + sequence.ToString());

            //sequence.SetSortStrategy(combsort);
            //sequence.Sort();
            //Console.WriteLine("After combsort:\n" + sequence.ToString());
            //2. zadatak:
            LinearSearch linearSearch = new LinearSearch();
            sequence.SetSearchStrategy(linearSearch);
            Console.WriteLine(sequence.Search(2110));

        }
    }
}
