﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prvi_zadatak
{
    //2. zadatak
    class LinearSearch : SearchStrategy
    {
        public override int Search(double number, double[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == number)
                    return i;
            }
            return -1;
        }

    }
}
