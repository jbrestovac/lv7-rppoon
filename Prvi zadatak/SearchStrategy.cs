﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prvi_zadatak
{
    abstract class SearchStrategy
    {
        public abstract int Search(double number, double[] arr);
    }
}
