﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Treci_zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider dataProvider = new SystemDataProvider();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            dataProvider.Attach(consoleLogger);

            while (1 < 2)
            {
                dataProvider.GetCPULoad();
                dataProvider.GetAvailableRAM();

                System.Threading.Thread.Sleep(2000);
            }
        }
    }
}
