﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace Treci_zadatak
{
    class ConsoleLogger : Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine(DateTime.Now + "CPU LOAD: " + provider.CPULoad + " Available RAM: " + provider.AvailableRAM);
        }
    }
}
